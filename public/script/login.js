var Login = function() {
    r = function() {

        $('input[type="text"],input[type="password"]').on({
            keypress: function() {$('.alert-danger').hide()},
            blur:function(){$('.alert-danger').hide()}
        }),

        $('#login').click(function(event) {
            if($('#user').val() === null || $('#user').val().length === 0 || /^\s+$/.test($('#user').val())){
                $('#user').focus();
                $("#error-users", $(".login-form")).show()
            }else if($('#password').val() === null || $('#password').val().length === 0 || /^\s+$/.test($('#password').val())){
                $('#password').focus();
                $("#error-pass", $(".login-form")).show();
            }else{

                var data_send = $('#frmlogin').serialize();
                $.post(base_url('/seguridad/users/validatelogin'),data_send, function(data, textStatus, xhr) {
                    if(data.status == 'success'){
                        window.location.href=base_url('/inicio');
                    }else if(data.status == 'error'){
                        $("#error-login", $(".login-form")).show();
                    }
                },'json');
            }

        }),

        $(document).keypress(function(e){
            if(e.keyCode == 13){
                e.preventDefault();
                $('#login').trigger('click');
            }
        }),

        $(".forget-form input").keypress(function(e) {
            return 13 == e.which ? ($(".forget-form").validate().form() && $(".forget-form").submit(), !1) : void 0
        }), $("#forget-password").click(function() {
            $(".login-form").hide(), $(".forget-form").show();
        }), $("#back-btn").click(function() {
            $(".login-form").show(), $(".forget-form").hide()
        }), $(".register-form input").keypress(function(e) {
            return 13 == e.which ? ($(".register-form").validate().form() && $(".register-form").submit(), !1) : void 0
        }), $("#register-btn").click(function() {
            $(".login-form").hide(), $(".register-form").show()
        }), $("#register-back-btn").click(function() {
            $(".login-form").show(), $(".register-form").hide()
        });
    };
    return {init: function() {r() } }
}();
$(document).ready(function() {
    Login.init()
});