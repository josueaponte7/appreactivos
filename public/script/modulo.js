$(document).ready(function() {

    var $folder = $('#folder').val();
    var $file   = $('#file').val();

    window.url_accion   = $folder + $file;
    window.url_buscar   = url_accion + '/buscar';
    window.url_eliminar = url_accion + '/eliminar';

    //$('select').select2();

    var $formulario = $(".formulario");
    var $guardar    = $('#guardar');

    $("#directorios").change(function () {
        var directory = $(this).val();
        $('#controller option:gt(0)').remove().end();
        $.get(base_url('seguridad/CModulo/getControllerajax'),{directory:directory}, function(data) {
            $('#controller').select2({'data':data});
        }, 'json');
    });

    $("#controller").change(function () {
        var al = $(this).val();
        $('#route').val('');
        if(al != null){
            var valor = al.replace(/([\/])?[C|c]/, function($0, $1){ return $1 ? $1+ '' : $0; }).toLowerCase();
            valor = valor.replace(/([\/])?[_]/, function($0, $1){ return $1 ? $1+ '' : $0; });
            if(valor != 0){
                $('#route').val(valor);
            }

            //$(this).val(al.replace(/([\/])?c/, function($0, $1){ return $1 ? $1+ 'C' : $0; }));
        }
    });

    /*$("#controller").change(function () {
        var al = $(this).val();
        $('#route').val('');
        if(al != null){
            var valor = al.replace('Controller','').toLowerCase();

            if(valor != 0){
                $('#route').val(valor);
            }

            //$(this).val(al.replace(/([\/])?c/, function($0, $1){ return $1 ? $1+ 'C' : $0; }));
        }
    });*/

    $formulario.validateForm();


    // $('.input-number').inputNumber();
});

