var PerfilManaged = function () {

    var p = function () {
        var url_app = window.location.pathname;

        var url_buscar = url_app+'/buscar';
        var url_eliminar = url_app+'/eliminar';

        var $forms = $("#frmmateria");
        var $guardar = $("#guardar");

        $forms.validateForm();
        var e = $("#tmateria");
        var p = e.DataTable({
            "language": {
                "url": public_url('js/es.json')
            },
            pagingType: "bootstrap_full_number",
            iDisplayLength: 5,
            iDisplayStart: 0,
            bStateSave: true,
            lengthMenu: [[5, 15, 20, -1], [5, 15, 20, "All"]],
            pageLength: 5,
            columnDefs: [
            {orderable: false, targets: [0, 1]},
            {searchable: false, targets: [0, 1]},
            {className: "dt-right"}
            ],
            order: [[1, "desc"]],
            aoColumns: [
            {"sWidth": "4%",},
            {"sWidth": "4%",},
            {"sWidth": "90%"},
            {"sWidth": "4%"}
            ]
        });

        $guardar.click(function(event) {
            $this = $(this);
            var perfil   = $('#perfil').val();
            var activo   = $(":radio[name=activo]:checked").parent("label").text().trim();
            var last_cod = $.getTableLastId(p);

            var check = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">';
            check += '<input type="checkbox" class="checkboxes" value="'+last_cod+'"/>';
            check += '<span></span></label>'
            var row = p.row( '.activo' ).node();
            var id  = $(row).attr('id' );
            var idx = $(row).index();
            if($.validar()){
                var data_send = $forms.serialize();
                if(id != undefined){
                    data_send = data_send +'&'+$.param({id:id});
                }
                var $url = url_app+'/'+$this.data('accion');
                $.guardar($url,data_send,$this.data('accion'),function(data){
                    if(data.success=='ok'){
                        bootbox.alert({
                            closeButton :false,
                            message: data.msg,
                            size: 'small',
                            callback: function(result){
                                location.reload();
                                /*if($this.data('accion') == 'guardar'){
                                    p.row.add([check, last_cod, perfil, activo]).draw();
                                    $.addLastIds(p,parseInt(last_cod));
                                }else{
                                    //console.log(p.cell(ids,1).data('Hola Mundo'))
                                    p.cell(parseInt(idx),2).data(perfil);
                                    p.cell(parseInt(idx),3).data(activo);
                                    $('#cancelar').trigger('click');
                                }*/
                            }
                        });
                    }else{
                        bootbox.alert({
                            closeButton :false,
                            message: data.msg,
                            size: 'small'
                        });
                    }
                });
            }
        });

        e.find('tbody tr').on('click', 'td:gt(0)', function () {
            // alert( p.cell( this ).data() );
            e.find('tbody tr').removeClass('activo')
            var idx  = p.cell(this).index().row;
            var ids  = p.row(idx).nodes().to$().attr('data-id');
            var id   = p.row(idx).nodes().to$().attr('id');
            var text = p.row(idx).nodes().to$().find('td:eq(1)').text();

            p.row(idx).nodes().to$().addClass('activo');

            $.buscar(url_buscar,{id:id},'', function(data, textStatus, xhr) {
                $.typeData(data)
                $('#guardar').attr({'disabled': true, 'data-accion':'modificar'});
            });
        });

        e.find(".group-checkable").change(function () {
            var e = jQuery(this).attr("data-set"),
            t = jQuery(this).is(":checked");
            jQuery(e).each(function () {
                t ? ($(this).prop("checked", !0), $(this).parents("tr").addClass("active")) : ($(this).prop("checked", !1), $(this).parents("tr").removeClass("active"))
            });
        }), e.on("change", "tbody tr .checkboxes", function () {
            $(this).parents("tr").toggleClass("active");
            $(this).parents("tr").toggleClass("activo");
        });

        $('#eliminar').click(function(e){
            e.preventDefault();
            $ids = [];
            var row    = p.row( '.activo' ).node();
            var id     = $(row).attr('id');
            var $count = p.rows('.activo').data().length;
            var data   = p.rows('.activo').data();
            var ids    = p.rows( '.activo' ).nodes().to$();
            $.each(ids, function(index, val) {
                $ids.push($(this).attr('id'));
            });
            if($count > 0){
                $.buscar(url_eliminar,{id:id},'eliminar', function(data, textStatus, xhr) {
                    if(data.success=='ok'){
                        bootbox.alert({
                            closeButton :false,
                            message: data.msg,
                            size: 'small',
                            callback: function(result){
                                location.reload();
                            }
                        });
                    }
                });
            }
        });

        var somethingChanged = false;
        $('#div-form').change(function(e) {
            somethingChanged = true;
        });

        $forms.change(function(e){
            var changedFieldset = $(e.target).parents('#div-form');
            if(changedFieldset.length > 0){
                $('#guardar').attr('disabled', false)
            }
        });

        $('#cancelar').click( function () {
            $('input[type="text"]').val('')
            $('input[name="activo"]').prop('checked',false).parent('label').removeClass('active');
            $('input[name="activo"][value="1"]').prop('checked',true).parent('label').addClass('active');
            p.rows('.activo').nodes().to$().removeClass('activo');
            p.search( '' ).columns().search( '' ).draw();
        });
    };

    return {
        init: function () {
            jQuery().DataTable && (p());
        }
    };
}();
App.isAngularJsApp() === !1 && jQuery(document).ready(function () {
    PerfilManaged.init();
});