$(document).ready(function(){
    $('input:checkbox').siblings('ul').addClass('hide');


    var TModulo = $('#tblmodulo').DataTable({
        "pagingType": "full_numbers",
        "language": {
            "url": assets_url('js/es.json')
        },
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "iDisplayLength": 20,
        "iDisplayStart": 0,
        "aoColumns": [
        {"sClass": "details-control", "sWidth": "2%"},
            // {"sClass": "right", "sWidth": "2%"},
            {"sClass": "left", "sWidth": "30%"},
            {"sClass": "left", "sWidth": "30%"},
            {"sWidth": "2%", "bSortable": false, "sClass": "center", "bSearchable": false}
            ],
            "createdRow" : function( row, data, index ) {
                if( data.hasOwnProperty("id") ) {
                    row.id = data.id;
                }
            }
        });

    $('i#arrow').on('click',function(){

        var $es = $(this);
        var $arrow_right = $es.hasClass('fa-arrow-right');
        if($arrow_right){

            $es.addClass('fa-arrow-down').removeClass('fa-arrow-right');
            $es.next('input:checkbox').next('img').next('span').next('ul').removeClass('hide').addClass('show');
        }else{

            $es.addClass('fa-arrow-right').removeClass('fa-arrow-down');
            $es.next('input:checkbox').next('img').next('span').next('ul').removeClass('show').addClass('hide');
        }
    });

    $("input.tri:checkbox").transformCheckbox({

        checked : assets_url("img/fancyForm/chk_on.png"),
        unchecked : assets_url("img/fancyForm/chk_off.png"),
        tristateHalfChecked : assets_url("img/fancyForm/chk_tri.png"),
        tristate : true
    });

    $('#tristate li ul').css('background','repeat-y url('+assets_url('img/fancyForm/treeview_ul.gif')+' 11px 2px')
    $('#tristate li ul li').css({'padding':'0 0 0 28px','background':'no-repeat url('+assets_url('img/fancyForm/treeview.gif')+') 11px 0'});
    $('.trans-element-checkbox.unchecked').css('background','no-repeat url(http://localhost/CISeguridad/assets/img/fancyForm/chk_off.png) center center');
    $('.trans-element-checkbox.checked').css('background','no-repeat url('+assets_url('img/fancyForm/chk_on.png')+') center center');


    var $forms = $("#frmpermiso");
    var $table = $('#tblpermiso');

    $('#perfil_id').change(function(){
        var $valor = $(this).val();
        $('#user_id').prop('disabled', false);
        if($valor > 0){
            $('#user_id').prop('disabled', true);
        }

    });

    $('#user_id').change(function(){
        var $valor = $(this).val();
        $('#perfil_id').prop('disabled', false);
        if($valor > 0){
            $('#perfil_id').prop('disabled', true);
        }
    });
    $('#guardar').click(function(){

        $perfil = $("#perfil_id").find('option').filter(':selected').val();
        $usuario = $("#user_id").find('option').filter(':selected').val();
        var nombreAnimate = 'animated shake';
        var finanimated   = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

        var $modulo_id = [];
        $('input:checkbox:checked').each(function(){
            var $padre_id = $(this).closest('ul').prev('span').prev('img').prev('input:checkbox').attr('id');;
            var $id = $(this).attr('id');
            var $parent = $(this).data('parent');
            if($padre_id != undefined){
                var existe = $modulo_id.indexOf($padre_id);
                if(existe < 0){
                    $modulo_id.push($padre_id)
                }
            }

            $modulo_id.push($id)
        })
        var marcados = $('input:checkbox:checked').length;

        if($perfil == 0 && $usuario == 0){
            bootbox.alert({
                closeButton :false,
                message: 'Debe seleccionar un perfil o un usuario',

            });
        }else if(marcados == 0){
            bootbox.alert({
                closeButton :false,
                message: 'Debe seleccionar un modulo',

            });
        }else{
            $('#perfil_id').prop('disabled', false);
            $('#user_id').prop('disabled', false);
            var data_send = $forms.serialize()+  '&' + $.param({modulo_id: $modulo_id});

            $.post(base_url('index.php/seguridad/permiso/agregar'),data_send, function(data){
                if(data.success=='ok'){
                    bootbox.alert({
                        closeButton :false,
                        message: data.msg,
                        callback: function(result){
                            location.reload();
                        }
                    });
                }
            },'json');
        }




    });






});