$(document).ready(function($) {
    $('input[type="text"], textarea, input:password').on({
        keypress: function() {
            $(this).parent('div').removeClass('has-error');
            $(this).parent('div').find('span').remove();
        }
    });
});
$.fn.addTable = function(a) {

};

$.edad = function(fecha_nac,par) {

    var fechaActual = new Date();
    var diaActual = fechaActual.getDate();
    var mmActual = fechaActual.getMonth() + 1;
    var yyyyActual = fechaActual.getFullYear();

    FechaNac = fecha_nac.split("-");
    var diaCumple = FechaNac[0];
    var mmCumple = FechaNac[1];
    var yyyyCumple = FechaNac[2];

    if (mmCumple.substr(0, 1) == 0) {
        mmCumple = mmCumple.substring(1, 2);
    }

    if (diaCumple.substr(0, 1) == 0) {
        diaCumple = diaCumple.substring(1, 2);
    }
    var edad = yyyyActual - yyyyCumple;

     if ((mmActual < mmCumple) || (mmActual == mmCumple && diaActual < diaCumple)) {
         edad--;
     }
     $("#"+par).val(edad);
 };

 (function($){
     $.fn.errorField = function() {
         var nombreAnimate = 'animated shake';
         var finanimated   = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         $(this).parent('div').addClass('has-error').addClass(nombreAnimate).one(finanimated,
             function () {
                 $(this).removeClass(nombreAnimate);
             });
         return $(this);
     };
 }(jQuery));

 var fields_require = [];
 (function($){
     $.fn.validateForm = function() {

         var $count_add     = $('[data-add]').length;
         var $count_row     = parseInt($('[data-column]').attr('data-column'));
         var $counter       = $('[data-counter]').length;
         // var $data_fields   = $('[data-counter]').length;

         /************ PENDIENTEEEEEEEEEEEE ********************/
        /*if($count_add != ($count_row+$counter)){
            alert('Error Initializate elements "data-add or data-column"');
            console.error('data-add:'+$count_add+' != data-column:'+($count_row+$counter));
        }*/
        $(this).find('[data-validate],[data-text],[data-type]').each(function(){
            var $id       = $(this).attr('id');
            var $validate = $(this).data('validate');
            var $text     = $(this).data('text');
            var $type     = $(this).data('type');
            var $mask     = $(this).data('mask');
            var rv  = [];
            if(/required/.test($validate)){
                fields_require.push($id);
            }

            var n = $validate.search("max");
            if(n > 0){
                var num = $validate.substring(n).match(/\d+/)[0];
                $(this).attr('maxlength',num);
            }
            if($type !== undefined){
                var h   = $type.match(/(numeric|integer|alpha|space|phone|float)/gi);

                $.each(h, function(key, value) {
                    if(value.toLowerCase() == 'alpha') {
                        rv[0] = value;
                    }else if(value.toLowerCase() == 'numeric'){
                        rv[1] = value;
                    }else if(value.toLowerCase() == 'space'){
                        rv[2] = value;
                    }
                });
                if(/^integer$/.test($type)){
                    $(this).mask("##########", {reverse: true}).css('text-align','right');
                }else if(/^numeric$/.test($type)){
                    $(this).mask("#.##0", {reverse: true}).css('text-align','right');
                } else if(/^phone$/.test($type)){
                    $(this).mask("(0000) 000-00-00");
                }else if(/^float$/.test($type)){
                    $(this).mask("#.##0,00", {reverse: true}).css('text-align','right');
                }

                if(/(numeric|alpha|space)/gi.test($type)){
                    var r = rv.join('');
                    $(this).fieldType(r);
                }
            }
            if($text !== undefined){
                var tt  = $text.match(/(upper|capitalize|lower)/gi);
                if(tt !== null){
                    $.each(tt, function(key, value) {
                        if(value.toLowerCase() == 'upper') {
                            $('#'+$id).keyup( function (e){
                                var str = $(this).val();
                                $(this).val(str.toUpperCase());
                            });
                        }else if(value.toLowerCase() == 'capitalize'){
                            $('#'+$id).keyup( function (e){
                                $(this).val($(this).val().charAt(0).toUpperCase()+$(this).val().slice(1));
                            });
                        }else{
                            $('#'+$id).keyup( function (e){
                                var str = $(this).val();
                                $(this).val(str.toLowerCase());
                            });
                        }
                    });
                }
            }
        });
    };
})(jQuery);

var arr_ids = [];
(function($){
    $.fn.fieldType = function(t) {
        var numeric  = '0123456789';
        var alpha    = '@abcdefghijklmnopqrstuvwxyzáéíóúñäëïöü_-,.'; // Se modifico esta linea
        var space    = ' ';
        var negative = '-';
        var b;

        if(t=='numeric'){
            b = numeric;
        }else if(t=='alpha'){
            b = alpha;
        }else if(t == 'alphaspace'){
            b = alpha+space;
        }else if(t== 'alphanumeric'){
            b = numeric+alpha;
        }else if(t== 'alphanumericspace'){
            b = numeric+alpha+space;
        }else if(t== 'numeric-'){
            b = numeric+negative;
        }else{
            b = t;
        }

        if(arr_ids.indexOf($(this).attr('id'))!=-1){
           alert('('+arr_ids[arr_ids.indexOf($(this).attr('id'))]+') fue inicializado mas de una vez');
           console.log('('+arr_ids[arr_ids.indexOf($(this).attr('id'))]+') fue inicializado mas de una vez');
        }
        arr_ids.push($(this).attr('id'));
        return this.each(function(p) {
            $(this).on({keypress: function(p) {
                var c = p.which, d = p.keyCode, e = String.fromCharCode(c).toLowerCase(), f = b;
                (-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || p.preventDefault();
            }});
        });
    };
})(jQuery);

$.extend({
    validar: function () {
        var i = 0;
        $.each(fields_require, function(key, value) {
            var c        = $('#'+value).data('validate');
            var n        = c.search("min");
            var e        = c.search("email");
            var m        = c.search("match");
            var em       = false; // variable para validar si existe correo
            var ta       = false;
            var ma       = false;
            var matcelem = 0;
            var selectval = 0;
            var email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(e>0){
                em = true;
            }
            var num;
            if(n > 0){
                num = c.substring(n).match(/\d+/)[0];
            }
            var arrytag = ["input", "select"];
            var tagnombre = $('#'+value).prop("tagName").toLowerCase();
            var a = arrytag.indexOf(tagnombre);
            if(a>0){
                selectval = $('#'+value).find('option').filter(':selected').val();
                ta=true;
            }
            /**** para obtener los valoeres dentro de parentesis ****/
            if(m > 0){
                matcelem = c.substring(m).match(/\(([^)]+)\)/)[1];
                ma=true;
            }
            if (($('#'+value).val() === null || $('#'+value).val().length === 0 || /^\s+$/.test($('#'+value).val()) || $('#'+value).val().length < num) && em === false) {
                if($('#'+value).val().length < num){
                    if ($("#span_error").length === 0 ) {
                        $('#'+value).parent('div').append('<span id="span_error" class="text-danger">min ('+num+') caracteres</span>');
                    }
                }
                $('#'+value).errorField();
                $('#'+value).focus();
                return false;

            }else if(ta == true && selectval == 0){
                $('#span_error').remove();
                $('#'+value).errorField();
                $('#'+value).focus();
                return false;
            }else if(em === true && !email.test($('#'+value).val())){
                $('#span_error').remove();
                $('#'+value).parent('div').append('<span id="span_error" class="text-danger">Email invalido</span>');
                $('#'+value).errorField();
                $('#'+value).focus();
                return false;
            }/*else if(ma === true && ($('#'+value).val()!=$('#'+matcelem).val())){
                $('#span_error').remove();
                $('#'+value+',#'+matcelem).parent('div').append('<span id="span_error" class="text-danger">Deben ser iguales</span>');
                $('#'+value+',#'+matcelem).errorField();
                return false;
            }*/else{
                i++;
            }
        });
        if(i == fields_require.length){
            /*console.log('hola');*/
            return true;
        }
    }
});

// $('.input-number').inputNumber();
/*
para user el plugin inputNumber() ->
<div class="div-input-number">
    <input type="text" class="form-control input-sm input-number" max="10" min="0" jump="3" length="2" begin="2">
</div>
*/
$.fn.inputNumber = function(a) {
    var e = $(this);
    var le = parseInt(e.attr('length'));
    var ma = parseInt(e.attr('max'));
    var mi = parseInt(e.attr('min'));
    var be = parseInt(e.attr('begin'));
    var ju = parseInt(e.attr('jump'));

    e.css({'text-align':'right'}).attr('maxlength',le).val(be);

    if(be > ma){
        e.val('');
        console.error('the begin value(%d) can not exceed the max(%d)',be,ma);
        return false;
    }
    if(mi > ma){
        console.error('the min value(%d) may not exceed the max(%d)',mi,ma);
    }

    if(be < mi){
        console.error('the begin value(%d) can not be less than the min(%d)',be,mi);
        e.val('').prop('disabled',true);
        return false;
    }
    var dmi = '';
    if(be == mi){
        dmi = 'disabled';
    }
    var dma = '';
    if(be == ma){
        dma = 'disabled';
    }

    e.wrapAll('<div class="input-group add-on" />');
    e.closest('div').append(' <div class="input-group-btn"><button class="btn btn-default btn-sm button-up" '+dma+' type="button"><i class="fa fa-angle-up"></i></button><button class="btn btn-default btn-sm button-down" '+dmi+' type="button"><i class="fa fa-angle-down"></i></button></div>');
    e.fieldType('numeric-');

    e.on('change',function(){
        if(e.val() > ma){
            console.error('this value(%d) can not exceed the max(%d)',e.val(),ma);
            $(this).val(be);
        }
        if(e.val() < mi){
            console.error('this value(%d) can not be less than the min(%d)',e.val(),mi);
            $(this).val(be);
        }
    });

    $(".div-input-number").on("click", "button.button-up", function (event) {
        $('.button-down').prop('disabled',false);
        var n  = parseInt(e.val());
        var es = $(this);
        var va = n+ju;
        if(va > ma){
            es.prop('disabled',true);
            return false;
        }
        e.val(va);
    });

    $(".div-input-number").on("click", "button.button-down", function (event) {
        $('.button-up').prop('disabled',false);
        var n  = parseInt(e.val());
        var es = $(this);
        var va = n-parseInt(ju);
        if(va < parseInt(mi)){
            es.prop('disabled',true);
            return false;
        }
        e.val(va);
    });
};


