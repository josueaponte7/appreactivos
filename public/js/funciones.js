$.addLastIds = function(table,ids){
    var $count = table.rows().data().length;
    var $last  = $count-1;
    table.row($last).nodes().to$().attr({'id':ids,'data-id':ids});
};

$.fn.getLastId = function(options) {
    var id = 1;
    if(options.id){
        id = 1 + parseInt(options.id);
    }else if(options.otable){
        id = 1 + parseInt(options.otable.row((options.otable.rows().data().length)-1).nodes().to$().attr('data-id'));
    }
    this.val(id);
    return this;
};

$.getTableLastId = function(otable) {
    var id    = 1;
    var total = otable.rows().count();
    var last_row = parseInt(otable.cell(total-1,1).data());
    var last_id = $.strpad(last_row+1, 2);
    return last_id;
};

$.clearCancel = function(options){
    $('input[type="text"]').not(function(){return ($.inArray($(this).attr('id'), options.noclear) > -1);}).val('');
    $('input[name="activo"]').prop('checked',false).parent('label').removeClass('active');
    $('input[name="activo"][value="1"]').prop('checked',true).parent('label').addClass('active');
    $('select').select2('val',0);
    $('#guardar').val('Guardar').attr({'data-action':'guardar','data-method':'agregar'});
    $('#id').getLastId(options);
    $('div').removeClass('has-error');
    $('div').find('span.text-danger').remove();
    $('#tblperfil_length').find('select').prop('selectedIndex',0).trigger('change');
    options.otable.search( '' ).columns().search( '' ).draw();
};

$.drawTable = function(options){
    if (options.action == "add") {

        // Cambiar informacion fila 0 columna 1
        // otable.cell(0,1).data('Hola Mundo');
        // IMPORTANTE OBTERNER EL ULTIMO ID LA DATATABLE
        // console.log(otable.row((otable.rows().data().length)-1).nodes().to$().attr('data-ids','100'));
        // console.log(otable.row((otable.rows().data().length)-1).nodes().to$().attr('data-ids'));

        var $edit = '<img alt="" src="'+assets_url(url_img+'modificar.png')+'" class="modificar mouse" data-id="current_id">';
        var $del  = '<img alt="" src="'+assets_url(url_img+'eliminar.png')+'" class="eliminar mouse" data-id="current_id">'  ;
        $edit     =  $edit.replace("current_id", options.current_id);
        $del      =  $del.replace("current_id", options.current_id);

        var array_val = [];

        if($('[data-counter]').length > 0){
            var $last_id   = 1 + options.otable.rows().count();
            array_val.push($last_id);
        }

        $('[data-add]').each(function(){
            console.log($(this).attr('data-add'));
            if($(this).prop('type') == 'text'){
                array_val.push($(this).val());
            }else if($(this).prop('type') == 'select'){
                array_val.push($(this).find('option').filter(':selected').text());
            }else if($(this).prop('type') == 'radio' || $(this).prop('type') == 'checkbox'){
                var $name = $(this).attr('name');
                array_val.push($('input:'+$(this).prop('type')+'[name="'+$name+'"]:checked').siblings('span').text());
            }
        });
        array_val.push($edit);
        array_val.push($del);
        array_val.push(array_val);
        options.otable.row.add(array_val).draw();
        $.addLastIds(options.otable,options.current_id);
    }else if (options.action == "mod") {
        $('[data-mod]').each(function(){
            var $column = $(this).data('mod');
            var $value  = '';
            if($(this).prop('type') == 'text'){
                $value = $(this).val();
            }else if($(this).prop('type') == 'select'){
                $value =  $(this).find('option').filter(':selected').text();
            }else if($(this).prop('type') == 'radio' || $(this).attr('type') == 'radio'){
                var $name = $(this).attr('name');
                $value = $('input:'+$(this).attr('type')+'[name="'+$name+'"]:checked').siblings('span').text();
            }
            options.otable.cell(options.row_action,$column).data($value).draw();
        });
    }else if(options.action == "del"){
        options.otable.row(options.thisrow).remove().draw();
        var total = options.otable.rows().count();
        for (var i = 0; i< total; i++) {
            options.otable.cell(i,0).data(i+1).draw();
        }
    }
    $('#id').getLastId({otable:options.otable});
    $('#cancelar').trigger('click');
};

$.searchData = function(data){
    $.each(data, function(i, valor) {
        var tipo = $('#'+i).prop('type');
        if(i == 'id'){
            $('#id').val(valor);
        }else  if(tipo !== undefined){
            if(tipo == 'text'){
                $('#'+i).val(valor);
            }else if(tipo == 'select'){
                $('#'+i).select2('val',valor);
            }
        }else{
            var tip = $('[name="'+i+'"]').prop('type');
            if(tip == 'radio'){
                var va = (valor=='f') ? 0 : 1;
                $('[name="'+i+'"]').parent('label').removeClass('active');
                $('[name="'+i+'"][value="'+va+'"]').prop('checked',true).parent('label').addClass('active');
            }
        }
    });
};

$.extend({
    guardar : function(url,data_send,action, callbackFnk){

        if(action == 'guardar'){
            $.post(base_url(url),data_send, function(data){
                if(typeof callbackFnk == 'function'){
                    callbackFnk.call(this, data,action);
                }
            },'json');
        }else{
            bootbox.confirm({
                size: 'small',
                closeButton :false,
                message: '<div style="text-align: center" class="text-danger">¿Desea modificar el registro?</div>',
                callback: function(result){
                    if(result){
                        $.post(base_url(url),data_send, function(data){
                            if(typeof callbackFnk == 'function'){
                                callbackFnk.call(this, data,action);
                            }
                        },'json');
                    }
                }
            });
        }
    },
    buscar : function(url, data_send, action, callbackFnk){
        if(action == 'eliminar'){
            bootbox.confirm({
                size: 'small',
                closeButton :false,
                message: '<div style="text-align: center" class="text-danger">¿Desea eliminar el registro?</div>',
                callback: function(result){
                    if(result){
                        $.get(base_url(url),data_send, function(data){
                            if(typeof callbackFnk == 'function'){
                                callbackFnk.call(this, data);
                            }
                        },'json');
                    }else{
                        $('#cancelar').trigger('click');
                    }
                }
            });
        }else{
            $.get(base_url(url),data_send, function(data){
                if(typeof callbackFnk == 'function'){
                    callbackFnk.call(this, data);
                }
            },'json');
        }
    }
});

// Devolver el tipo de dato de una clave dada
$.fn.hasData = function(key) {
    return (typeof $(this).data(key) != 'undefined');
};

// Añadir ceros a la izquierda pasando el primer parametro el numero y el segundo la cantidad de ceros

$.strpad = function(number, length){
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
};

$.typeData = function(data){
    $.each(data, function(i, valor) {
        if(i != 'id'){
            var obj = $("[name='"+i+"']");
            var tag_name = obj.get(0).tagName.toLowerCase();
            var tipo = obj.attr('type');
            if(tipo == 'text'){
                obj.val(valor)
            }else if(tipo == 'radio'){
                obj.parent('label').removeClass('active');
                objr = $('[name="'+i+'"]');
                objr.removeAttr("checked");
                $('[name="'+i+'"][value="'+valor+'"]').attr('checked',true).parent('label').addClass('active');
            }
        }
    });
}

// para sumar filas de datatable
jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
    return this.flatten().reduce( function ( a, b ) {
        if ( typeof a === 'string' ) {
            a = a.replace(/[^\d.-]/g, '') * 1;
        }
        if ( typeof b === 'string' ) {
            b = b.replace(/[^\d.-]/g, '') * 1;
        }

        return a + b;
    }, 0 );
} );
/*$('.tbl-table tbody').on( 'click', 'tr', function (event) {
    var $elemento = $(event.target);
    $elemento = $elemento.is('img') ? 1 : 0;
    if($elemento === 0){
        $(this).toggleClass('selected');
    }
});*/


