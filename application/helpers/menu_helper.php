<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('menu')) {
    function menu() {
        $ci = & get_instance();
        $menu = $ci->libreria->menu();
        return $menu;

    }
}