<?php
/**
*
*/
class MUsers extends SI_Model
{
    private $table = 'se_users';
    function __construct()
    {
        parent::__construct();
    }
    public function listar()
    {
        $perfil_id = $this->session->userdata('perfil_id');

        $this->db->select("u.id, u.si_user, u.first_name, u.last_name, u.correo, u.active, p.perfil");
        $this->db->from($this->table.' AS u');
        $this->db->join('se_perfil AS p', 'u.perfil_id = p.id');
        if($perfil_id > 1){
            $this->db->where('u.perfil_id > 2');
        }
        $query = $this->db->get();
        return $query->result();
    }
    public function agregar($data)
    {
        $insert = $this->insert($this->table, $data, $this->_unique);
        return $insert;
    }

    public function modificar($id,$data)
    {
        $this->db->where('id', $id);
        $result = $this->db->update($this->table,$data);
        return $result;
    }

    public function login($user,$pass)
    {
        $checkpass = $this->libreria->checkPassword($user, $pass);
        return $checkpass;
    }

    public function changePassword($id,$data)
    {

        $user = strtolower($this->libreria->getName());
        $login = $this->login($user,$this->input->get('password_f'));
        if (strlen($login) > 0) {
            $this->db->where('id', $id);
            $datos['si_password'] = $this->libreria->generatePassword($this->input->get('clave_new'));
            $this->db->update($this->table, $datos);
            echo 1;
        } else {
            echo 2;
        }
    }

    public function userActive()
    {
        $this->db->select("id, si_user");
        $this->db->where('active',TRUE);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function buscar($id)
    {
        $this->db->select('id, si_user, first_name, last_name, correo, show_panel, active, perfil_id, change_password');
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function api_visita()
    {
        $data['fecha_creacion'] = date('Y-m-d');
        $this->db->insert("visita_api", $data);
    }


}