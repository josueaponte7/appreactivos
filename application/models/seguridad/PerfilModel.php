<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
*
*/
include(APPPATH.'models/Perfil.php');
class PerfilModel extends SI_Model
{
    private $table = 'se_perfil';
    public $_unique = ['perfil'];
    function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {

        $this->db->select("id, perfil, activo");
        $this->db->order_by("id",'asc');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function listUser()
    {
        $this->db->select("id, perfil, activo")
        ->where_not_in('id', array(1,3));
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function agregar($data)
    {
        $insert = $this->insert($this->table, $data, $this->_unique);
        return $insert;
    }

    public function buscar($id)
    {
        $this->db->select('id, perfil, activo');
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        return $query->row();
    }
    public function modificar($id,$data)
    {
        $result = $this->db->where('id !=', $id);
        $result = $this->db->where('perfil =', $data['perfil']);
        $result = $this->db->get($this->table);

        if ($result->num_rows() > 0) {
            #echo "CORRECTO";
            #echo '1';
        } else {
            $this->db->where('id', $id);
            $result = $this->db->update($this->table,$data);
            return $result;
        }
    }
    public function eliminar($id)
    {
        $result = $this->db->where_in('perfil_id', $id);
        $result = $this->db->get('se_users');
        if ($result->num_rows() > 0) {
            #echo "CORRECTO";
            #echo '1';
        } else {
            //$datos['id'] = $id;
            $this->db->where_in( 'id', $id );
            return $this->db->delete($this->table);
        }
    }

    public function perfil_active()
    {

        $perfil_id = $this->session->userdata('perfil_id');
        $this->db->select("id, perfil");
        $this->db->where('activo',1);
        if($perfil_id > 1){
            $this->db->where("id > 2");
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
}