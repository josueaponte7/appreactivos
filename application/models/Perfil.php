<?php
/**
*
*/
class Perfil
{

    public $id;
    public $perfil;
    public $activo;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;
    }
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    public function getId()
    {
        return $this->id;
    }
}