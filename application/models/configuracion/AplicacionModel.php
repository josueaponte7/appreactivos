<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*/
class AplicacionModel extends SI_Model
{
    private $table = 'aplicacion';
    function __construct()
    {
        parent::__construct();
    }
    public function listar()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }
    public function agregar($data)
    {
        $insert = $this->insert($this->table, $data, $this->_unique);
        return $insert;
    }

    public function modificar($id,$data)
    {
        $this->db->where('id', $id);
        $result = $this->db->update($this->table,$data);
        return $result;
    }

    public function buscar($id)
    {
        $this->db->select('id, aplicacion, activo');
        $this->db->where('id',$id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function eliminar($id)
    {
        $this->db->where_in( 'id', $id );
        return $this->db->delete($this->table);
    }
}
