<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*/
class Access
{
    private $ci;
    private $allowed_controller;
    private $allowed_method;
    private $disallowed_method;
    function __construct()
    {

        $this->ci =& get_instance();
        $this->allowed_controller = ["cusers"];
        $this->disallowed_method  = ["login","validateLogin"];
        if(!isset($this->ci->session)) {
            $this->ci->load->library('session');
        }
        $this->ci->load->database();
    }
    public function _isLoggedIn()
    {

        $class   = $this->ci->router->class;
        $method  = $this->ci->router->method;
        $session = $this->ci->session->userdata('logged_in');

        if(isset($session) && !in_array($class,$this->allowed_controller)){
            if(in_array($method,$this->disallowed_method)){
                $this->ci->db->select("slug");
                $this->ci->db->where("index_route",true);
                $query = $this->ci->db->get('se_app_routes',1);
                $route = $query->row()->slug;
                $route = $route;
                redirect($route);
            }
        }
    }

}