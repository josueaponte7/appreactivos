<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*/
class UnAccess
{
    private $ci;
    private $allowed_controller;
    private $allowed_method;
    private $disallowed_method;
    function __construct()
    {

        $this->ci =& get_instance();
        $this->ci->load->model('seguridad/MAcceso','acceso');
        $controllers = $this->ci->acceso->listar_controllers();

        $this->allowed_controller = ["cusers", 'chatcontroller'];
        $this->allowed_method     = ["login","validateLogin", 'index', 'send', 'guardar', 'usuario', 'conectar'];

        foreach ($controllers as $controller){
            $controlle = strtolower($controller->controllers);
            array_push($this->allowed_controller, $controlle);
            $methods =  $this->ci->acceso->listar_methods($controller->controllers);
            $method = explode(",",$methods->methods);
            foreach ($method as $metho) {
                array_push($this->allowed_method, $metho);
            }
        }
        if(!isset($this->ci->session)) {
            $this->ci->load->library('session');
        }
        $this->ci->load->database();
    }

    public function _istLoggedIn()
    {

        $class   = $this->ci->router->class;
        $method  = $this->ci->router->method;
        $session = $this->ci->session->userdata('logged_in');

        if(empty($session) && !in_array($class,$this->allowed_controller)){
            if(!in_array($method,$this->allowed_method)){
                $this->ci->db->select("slug");
                $this->ci->db->where("default_route",true);
                $query = $this->ci->db->get('se_app_routes',1);
                if($query->num_rows() > 0){
                    $route = $query->row->slug;
                    $route = $query->row()->slug;
                    redirect($route);
                }else{
                    if($class != $this->ci->router->default_controller){
                       redirect(base_url());
                    }
                }
            }
        }
    }
}