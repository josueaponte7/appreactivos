<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HomeController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
        redirect('/seguridad/users/login', 'refresh');
        // $data['titulo'] = 'Desde Inicio';
        // $this->twig->display('inicio/inicio', $data);
    }

    public function example()
    {
        $data['titulo'] = 'Desde Inicio';
        $this->twig->display('inicio/home', $data);
    }

    public function prueba()
    {

    }
    public function metadata()
    {
        $table_metadata = "estructura_data.json";
        if (file_exists($table_metadata)) {
            $j = file_get_contents($table_metadata);
            $products = json_decode($j);

            $table = $products->table;
            unset($products->table);
            foreach ($products as $key => $product) {
                foreach ($product as $item) {
                    echo '<br/>' . $item->name . "<br/>";
                    echo $item->metadata->tipo . "<br/>";
                    echo isset($item->metadata->tamano) ? $item->metadata->tamano : NULL."<br/>";
                    echo isset($item->metadata->key) ? $item->metadata->key : NULL . "<br/>";

                }
            }
        } else {
            echo 'error';
        }
    }

    public function recursiva()
    {
        print_r($this->showreply($id=0));
        //$this->listar_directorios_ruta("./application/controllers/");
    }

    public function modulos_rec($id)
    {
        $this->db->select('id, modulo');
        $this->db->where('modulo_id', 0);
        $query = $this->db->get('se_modulo');
        $datos = $query->result();
        return $datos;
    }

    public function showreply($reply_id, $id = array()) {
        $q1 =$this->db->select('id, modulo, modulo_id')
        ->from('se_modulo')
        ->where('modulo_id', 0)
        ->or_where('modulo_id',$reply_id)
        ->order_by('id ')->get();;

        foreach( $q1->result_array() as $row4 ) {
            array_push($id, $row4);
            $parent_id = $row4['id'];
            $this->showreply($row4['id'], $id);

        }

       return  $id; //here want to return result
   }

}