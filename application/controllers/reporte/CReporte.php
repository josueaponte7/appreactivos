<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CReporte extends SI_Controller
{

    private $dir   = 'reporte/';
    private $files = array('new'=>'reporte','generacion'=>'factura');
    private $vista = '';
    #private $table = 'mp_reporte';
    private $titulo = 'Generación Reporte(s)';

    public function __construct()
    {
        parent::__construct();
        $this->files = (object)$this->files;
        $this->load->model('reporte/MReporte','reporte');
        $this->load->model('registro/MContribuyente','factura');
        //echo strtolower(substr($this->router->fetch_class(),1));
    }
    public function index()
    {
        $datos['folder'] = $this->dir;
        $datos['file']   = $this->files->new;
        $datos['titulo'] = $this->titulo;
        $datos['factura']  = $this->factura->listar();
        $this->vista = $this->dir.$this->files->new;
        $this->template->write_view('content', $this->vista,$datos);
        $this->template->render();
    }

    public function comprobante_factura()
    {
        $id = $this->input->get('id');
        $datos = $this->reporte->comprobante_factura($id);
        $this->load->view('reporte/factura', $datos);
    }

    public function comprobante_remision()
    {
        $id = $this->input->get('id');
        $datos = $this->reporte->comprobante_remision($id);
        $this->load->view('reporte/remision', $datos);
    }

    public function comprobante_retencion()
    {
        $id = $this->input->get('id');
        $datos = $this->reporte->comprobante_retencion($id);
        $this->load->view('reporte/retencion', $datos);
    }
}
