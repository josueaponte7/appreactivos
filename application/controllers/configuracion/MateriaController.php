
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MateriaController extends SI_Controller {

    private $_dir    = '';
    private $_files  = array('new' => 'materia');
    private $_vista  = '';
    private $_table = 'materia';
    private $titulo = 'Registro de Materia';
    protected $_not_check = ['user_create', 'date_create', 'user_update', 'date_update'];
    private $_controller = 'materia';

    public function __construct()
    {
        parent::__construct();
        $this->_tab        = $this->_table;
        $this->_dir        = $this->router->directory;
        // $this->_controller =  $this->router->fetch_class();

        $this->_files = (object) $this->_files;
        $this->load->model('configuracion/MateriaModel', 'materia');
    }

    public function index()
    {
        $this->_vista       = $this->_dir . $this->_files->new;

        $this->_id         = $this->libreria->getModuleID($this->router->directory . $this->router->class);
        $this->_padre_id   = $this->libreria->getPadreID($this->_id);
        $datos['_id']      = $this->_id;
        $datos['padre_id'] = $this->_padre_id;
        $datos['token']    = $this->libreria->token();
        $datos['titulo']   = $this->titulo;
        $datos['listar']   = $this->materia->listar();

        $this->_vista = $this->_dir.$this->_files->new;
        $this->twig->display($this->_vista, $datos);
    }

    public function guardar()
    {

        $data = $this->input->post();
        $response_data['success'] = 'error';

        if($this->_validate_form($data)){
            $result = $this->materia->agregar($data);
            if($result['unique_key']){
                $response_data['success'] = 'existe';
                $response_data['msg'] = '<div>Ya existe una materia con ese nombre</div>';
            }else{
                $response_data['success'] = 'ok';
                $response_data['msg'] = '<div>Registro exitoso</div>';
            }
        }
        echo json_encode($response_data);
    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $resultado = $this->materia->buscar($id);
        echo json_encode($resultado);
    }

    public function modificar()
    {
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['csrf_test_name']);
        unset($data['token']);
        unset($data['id']);

        $result = $this->materia->modificar($id, $data);
        if ($result) {
            $response_data['success'] = 'ok';
            $response_data['msg'] = '<div>Registro modificado con exito</div>';
            $response_data['action'] = 'update';
        } else {
            $response_data['success'] = 'existe';
            $response_data['msg'] = '<div>Ya existe un perfil con ese nombre</div>';
        }

        echo json_encode($response_data);
    }

    public function eliminar()
    {
        $response_data['success'] = 'error';
        $id = $this->input->get('id');
        $resultado = $this->materia->eliminar($id);
        if ($resultado) {
            $response_data['success'] = 'ok';
            $response_data['msg'] = '<div>Registro eliminado con exito</div>';
        }else{
            $response_data['success'] = 'error';
            $response_data['msg'] = '<div>Disculpe, el registro no se puede eliminar se encuentra asociado a uno o más elementos</div>';
        }
        echo json_encode($response_data);
    }
}

/* End of file UsuarioController.php */
/* Location: ./application/controllers/UsuarioController.php */