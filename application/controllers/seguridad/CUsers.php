<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author josue
 */
class CUsers extends SI_Controller
{

    private $_dir    = '';
    private $_files  = array('new' => 'users', 'list' => 'users', 'login' => 'login', 'lock' => 'lock', 'perfil' => 'perfil');
    private $_vista  = '';
    private $_table  = 'se_users';
    private $titulo = 'Registro de Usuario';
    protected $_not_check = ['user_create', 'date_create', 'user_update', 'date_update'];

    public function __construct()
    {

        parent::__construct();
        $this->_tab = $this->_table;
        $this->_dir = $this->router->directory;
        $this->_files = (object) $this->_files;
        $this->load->model('seguridad/MUsers', 'users');
        $this->load->model('seguridad/PerfilModel', 'perfil');
    }

    public function index()
    {

        $this->_vista       = $this->_dir . $this->_files->new;

        $this->_id         = $this->libreria->getModuleID($this->router->directory . $this->router->class);
        $this->_padre_id   = $this->libreria->getPadreID($this->_id);
        $datos['_id']      = $this->_id;
        $datos['padre_id'] = $this->_padre_id;
        $datos['token']    = $this->libreria->token();
        $datos['perfiles'] = $this->perfil->perfil_active();
        $datos['titulo']   = $this->titulo;
        $datos['listar']   = $this->users->listar();

        $this->_vista = $this->_dir.$this->_files->new;
        $this->twig->display($this->_vista, $datos);
    }

    public function guardar()
    {

        $config = Array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'sisadmidesarrollo@gmail.com',
            'smtp_pass' => 'administradordesarrollo',
            'mailtype'  => 'html',
            'charset'   => 'UTF-8',
            'wordwrap'  => TRUE
            );

        $data       = $this->input->post();
        $si_user    = $data['si_user'];
        $si_pasword = $data['si_password'];

        $this->email->initialize($config);
        $this->email->from('sisadmidesarrollo@gmail.com');
        $this->email->to($data['correo']);
        $this->email->subject('Cuenta de usuario');
        $this->email->message("<div>El usuario  <span style='font-weight:bold'>$si_user</div> fue creado su clave es <span style='font-weight:bold'>$si_pasword</span></div><div><a href='" . base_url() . "' >Acceder al sistema</a></div>");
        $pass                = $this->libreria->generatePassword($data['si_password']);
        $data['si_password'] = $pass;
        $result              = $this->users->agregar($data);
        if ($result) {
            $response_data['success'] = 'ok';
            $response_data['msg']     = '<div>Registro exitoso</div>';
            $response_data['action']  = 'save';
            $this->email->send();
        } else {
            $response_data['success'] = 'error';
            $response_data['msg']     = '<div>Registro existe</div>';
        }
        $resul_activity = $this->libreria->generateActivity('Registro de Usuario');

        echo json_encode($response_data);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $config                   = Array(
                'protocol'  => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'sisadmidesarrollo@gmail.com',
                'smtp_pass' => 'administradordesarrollo',
                'mailtype'  => 'html',
                'charset'   => 'UTF-8',
                'wordwrap'  => TRUE
                );
            $response_data['success'] = 'error';
            $data                     = $this->input->post();
            $id                       = $data['id'];
            unset($data['csrf_test_name']);
            unset($data['token']);
            unset($data['si_rpassword']);
            $si_user                  = $data['si_user'];
            $si_pasword               = $data['si_password'];
            $this->email->initialize($config);
            $this->email->from('sisadmidesarrollo@gmail.com');
            $this->email->to($data['correo']);
            $this->email->subject('Cuenta de usuario');
            $this->email->message("<div>El usuario  <span style='font-weight:bold'>$si_user</div> fue creado su clave es <span style='font-weight:bold'>$si_pasword</span></div><div><a href='" . base_url() . "' >Acceder al sistema</a></div>");
            $pass                     = $this->libreria->generatePassword($data['si_password']);
            $data['si_password']      = $pass;
            $result                   = $this->users->modificar($id, $data);
            if ($result) {
                $response_data['success'] = 'ok';
                $response_data['msg']     = '<div>Registro modificado con exito, <span class="text-danger"> la p&aacute;gina se actualizara para realizar los cambios</span></div>';
            }
        } else {
            $response_data['success'] = 'error';
        }
        echo json_encode($response_data);
    }

    public function buscar()
    {
        $id        = $this->input->get('id');
        $resultado = $this->users->buscar($id);
        echo json_encode($resultado);
    }

    public function logout()
    {
        $resul_activity = $this->libreria->generateActivity('Cerro Sesi&oacute;n');
        if ($resul_activity) {
            if (isset($this->session)) {
                $array_sesiones = array('perfil_id' => '', 'user_id' => '', 'logged_in' => '');
                $this->session->unset_userdata($array_sesiones);
                $this->session->sess_destroy();
                $this->removeCache();
            }
            $this->db->select("slug");
            $this->db->where("default_route", true);
            $query = $this->db->get('se_app_routes', 1);
            if($query->num_rows() > 0){
                $route = $query->row->slug;
                $route = $query->row()->slug;
                redirect($route);
            }else{
                redirect(base_url());
            }
        }
    }

    public function lockScreen()
    {

        $datos['name'] = strtoupper($this->libreria->getNamesUser()->si_user);
        $datos['user'] = strtoupper($this->libreria->getNamesUser()->si_user);
        $this->vista = $this->dir.$this->files->lock;
        $this->twig->display($this->vista, $datos);
    }

    public function perfil()
    {
        $this->vista = $this->dir.$this->files->perfil;
        $this->twig->display($this->vista);
    }
    public function login()
    {

        $this->_vista = $this->_dir . $this->_files->login;
//        $datos['name'] = $this->security->get_csrf_token_name();
        $datos['token'] = $this->libreria->token();

        $this->twig->display($this->_vista, $datos);
    }

    public function validateLogin()
    {

        $visita = $this->input->get('visita');
        if ($visita == 1) {
            $this->users->api_visita();
        } else {
            if ($this->input->is_ajax_request()) {
                if ($this->input->input_stream('user', TRUE) && $this->input->input_stream('token') == $this->session->userdata('token')) {

                    /*$user = $this->input->input_stream('user', TRUE); // utilizar metodos put o patch
                    if ($this->security->xss_clean($user)) {
                        echo 'paso';
                    }*/
                    $data_response['status'] = 'error';
                    $user = $this->input->input_stream('user');
                    $pass = $this->input->input_stream('password');

                    $result = $this->users->login($user, $pass);
                    if ($result) {
                        $resul_activity = $this->libreria->generateActivity('Inicio de Sesi&oacute;n');
                        $this->session->userdata('user_id');
                        if ($resul_activity) {
                            $data_response['status'] = 'success';
                        }
                    }
                    echo json_encode($data_response);
                } else {
                    echo 'error1';
                }
            } else {
                echo 'error2';
            }
        }
    }

    public function changePassword()
    {

        $id                  = $this->session->userdata('user_id');
        $pass                = $this->libreria->generatePassword($this->input->get('clave_new'));
        $data['si_password'] = $pass;
        $this->users->changePassword($id, $data);
    }

}
