<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of Modulo
 *
 * @author josue
 */

class CModulo extends SI_Controller
{

    private $_dir      = '';
    private $_files    = array('new'=>'modulo','list'=>'modulo');
    private $_vista    = '';
    private $table     = 'se_modulo';
    private $_id       = 0;
    private $_padre_id = 0;

    public function __construct()
    {
        parent::__construct();
        $this->_dir = $this->router->directory;
        $this->_files = (object)$this->_files;
        $this->load->model("seguridad/MModulo", "modulo");

       //strtolower(substr($this->router->class,1));
       // echo $this->router->method;
       // echo $this->router->directory;
    }

    public function tabla_ajax()
    {
     echo 'gggg';
    }

    public function getControllerajax()
    {
        $directory = $this->input->get('directory');
        $controllers = $this->modulo->getControllers($directory);
        echo json_encode($controllers);
    }
    public function index()
    {

        $this->_id            = $this->libreria->getModuleID($this->router->directory . $this->router->class);
        $this->_padre_id      = $this->libreria->getPadreID($this->_id);
        $datos['_id']         = $this->_id;
        $datos['padre_id']    = $this->_padre_id;
        $datos['folder']      = $this->_dir;
        $datos['file']        = $this->_files->new;
        $datos['animation']   = 'zoomIn';
        $datos['titulo']      = 'Registro de Modulos';
        $datos['id']          = $this->modulo->lastId($this->table);
        $datos['lista']       = $this->modulo->listar();
        $datos['modulos']     = $this->modulo->modulo();
        $datos['token']       = $this->libreria->token();
        $datos['directories'] = $this->modulo->getDiretories();

        $this->_vista = $this->_dir.$this->_files->new;

        $this->twig->display($this->_vista, $datos);

    }

    public function guardar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            unset($data['csrf_test_name']);
            unset($data['token']);
            $response_data['success']='error';
            $result = $this->modulo->agregar($data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro exitoso, <span class="text-danger"> la p&aacute;gina se actualizara para realizar los cambios</span></div>';
            }
        } else {
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }

    public function buscar()
    {
        $id = $this->input->get('id');
        $resultado = $this->modulo->buscar($id);
        echo json_encode($resultado);
    }

    public function modificar()
    {
        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
            $data = $this->input->post();
            $id = $data['id'];
            unset($data['csrf_test_name']);
            unset($data['token']);
            unset($data['id']);

            $response_data['success']='error';
            $result = $this->modulo->modificar($id, $data);
            if($result){
                $response_data['success']='ok';
                $response_data['msg']='<div>Registro modificado con exito, <span class="text-danger"> la p&aacute;gina se actualizara para realizar los cambios</span></div>';
            }
        } else {
            $response_data['success']='error';
        }
        echo json_encode($response_data);
    }

    public function eliminar()
    {
        $id = $this->input->get('id');
        $this->modulo->eliminar($id);
    }

}