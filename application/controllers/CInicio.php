<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CInicio extends SI_Controller
{

    private $dir   = array('sistema' => 'seguridad', 'inicio'=>'inicio');
    private $files = array('inicio' => 'inicio', 'sistema' => 'sistema');
    private $vista = '';
    private $table = 'ticket';

    public function __construct()
    {
        parent::__construct();
        $this->dir   = (object) $this->dir;
        $this->files = (object) $this->files;
    }

    public function index()
    {
        $this->vista = $this->dir->sistema . '/' . $this->files->sistema;
        $perfil_id = $this->session->userdata('perfil_id');
        if($perfil_id > 1){
             $this->vista = $this->dir->inicio . '/' . $this->files->inicio;
        }
        $datos['perfil'] = $this->libreria->getPerfil();

        $this->twig->display($this->vista, $datos);
    }
}
