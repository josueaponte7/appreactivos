<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$hook['pre_system'][] = array(
    'class' => 'SiteOffline',
    'function' => 'offline',
    'filename' => 'SiteOffline.php',
    'filepath' => 'hooks'
);

$hook['post_controller_constructor'][] = array(
    'class'    => 'Access',
    'function' => '_isLoggedIn',
    'filename' => 'Access.php',
    'filepath' => 'hooks',
    'params'   => array()
);

$hook['post_controller_constructor'][] = array(
    'class'    => 'UnAccess',
    'function' => '_istLoggedIn',
    'filename' => 'UnAccess.php',
    'filepath' => 'hooks',
    'params'   => array()
);


/*$hook['post_controller_constructor'][] = array(
    'class' => 'ValidateController',
    'function' => 'sessionActive',
    'filename' => 'ValidateController.php',
    'filepath' => 'hooks'
);*/
/*$hook['pre_system'][] = array(
    'class' => 'ValidateController',
    'function' => 'validateController',
    'filename' => 'ValidateController.php',
    'filepath' => 'hooks'
);*/