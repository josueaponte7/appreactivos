<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController';
$route['404_override'] = 'Error404';
$route['translate_uri_dashes'] = FALSE;


require_once(BASEPATH . 'database/DB.php');
$db = &DB();
$db->select('slug, controller');
$query = $db->get('se_app_routes');
$result = $query->result();
foreach ($result as $row) {
    $route[$row->slug] = $row->controller;
    $route[$row->slug . '/agregar'] = $row->controller . '/agregar';
    $route[$row->slug . '/modificar'] = $row->controller . '/modificar';
    $route[$row->slug . '/eliminar'] = $row->controller . '/eliminar';
}

$query = $db->select('route, controller, accion')->where("accion !=''")->get('se_modulo');
$result = $query->result();

foreach ($result as $row) {
    if ($row->route != '') {

        list($guardar, $modificar, $eliminar, $buscar) = explode(',', $row->accion);
        $route[$row->route] = $row->controller;
        $route[$row->route . '/' . $guardar] = $row->controller . '/' . $guardar;
        $route[$row->route . '/' . $modificar] = $row->controller . '/' . $modificar;
        $route[$row->route . '/' . $eliminar] = $row->controller . '/' . $eliminar;
        $route[$row->route . '/' . $buscar] = $row->controller . '/' . $buscar;

    }
}

$route['seguridad/users/perfil'] = 'seguridad/CUsers/perfil';
$route['seguridad/users/lockscreen'] = 'seguridad/CUsers/lockscreen';
$route['seguridad/perfil/tabla_ajax'] = 'seguridad/PerfilController/tabla_ajax';