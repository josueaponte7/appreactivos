<?php

/* base/menu.twig */
class __TwigTemplate_fdc086ce9aeb9b2b4e3bc4f58991d867f86ea1d635ffa609f422ad80de3f95bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page-sidebar-wrapper\">
    <div class=\"page-sidebar navbar-collapse collapse\">
        <ul class=\"page-sidebar-menu   \" data-keep-expanded=\"false\" data-auto-scroll=\"true\" data-slide-speed=\"200\">
            ";
        // line 7
        echo "
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(menu());
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 9
            echo "                ";
            $context["activo_p"] = "";
            // line 10
            echo "                ";
            $context["row_open"] = "";
            // line 11
            echo "                ";
            if (($this->getAttribute($context["menu"], "id", array()) == (isset($context["padre_id"]) ? $context["padre_id"] : null))) {
                // line 12
                echo "                    ";
                $context["activo_p"] = "active open";
                // line 13
                echo "                    ";
                $context["row_open"] = "open";
                // line 14
                echo "                ";
            }
            // line 15
            echo "                <li class=\"nav-item ";
            echo twig_escape_filter($this->env, (isset($context["activo_p"]) ? $context["activo_p"] : null), "html", null, true);
            echo "\">
                    <a href=\"javascript:;\" class=\"nav-link nav-toggle\">
                        <i class=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["menu"], "icono", array()), "html", null, true);
            echo "\"></i>
                        <span class=\"title\">";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["menu"], "modulo", array()), "html", null, true);
            echo "</span>
                        <span class=\"arrow ";
            // line 19
            echo twig_escape_filter($this->env, (isset($context["row_open"]) ? $context["row_open"] : null), "html", null, true);
            echo "\"></span>
                    </a>
                    <ul class=\"sub-menu\">
                        ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(submenu($this->getAttribute($context["menu"], "id", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["submenu"]) {
                // line 23
                echo "                            ";
                $context["activo"] = "";
                // line 24
                echo "                            ";
                if (($this->getAttribute($context["submenu"], "id", array()) == (isset($context["_id"]) ? $context["_id"] : null))) {
                    // line 25
                    echo "                                ";
                    $context["activo"] = "active open";
                    // line 26
                    echo "                            ";
                }
                // line 27
                echo "
                            <li class=\"nav-item ";
                // line 28
                echo twig_escape_filter($this->env, (isset($context["activo"]) ? $context["activo"] : null), "html", null, true);
                echo "\">
                                <a href=\"";
                // line 29
                echo twig_escape_filter($this->env, (base_url() . $this->getAttribute($context["submenu"], "route", array())), "html", null, true);
                echo "\" class=\"nav-link \">
                                    <span class=\"title\">";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["submenu"], "modulo", array()), "html", null, true);
                echo "</span>
                                </a>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submenu'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "                    </ul>
                </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "        </ul>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "base/menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 37,  106 => 34,  96 => 30,  92 => 29,  88 => 28,  85 => 27,  82 => 26,  79 => 25,  76 => 24,  73 => 23,  69 => 22,  63 => 19,  59 => 18,  55 => 17,  49 => 15,  46 => 14,  43 => 13,  40 => 12,  37 => 11,  34 => 10,  31 => 9,  27 => 8,  24 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"page-sidebar-wrapper\">
    <div class=\"page-sidebar navbar-collapse collapse\">
        <ul class=\"page-sidebar-menu   \" data-keep-expanded=\"false\" data-auto-scroll=\"true\" data-slide-speed=\"200\">
            {#<li class=\"heading\">
                <h3 class=\"uppercase\">Features</h3>
            </li>#}

            {% for menu in menu() %}
                {% set activo_p = '' %}
                {% set row_open = '' %}
                {% if menu.id == padre_id %}
                    {% set activo_p = 'active open' %}
                    {% set row_open = 'open' %}
                {% endif %}
                <li class=\"nav-item {{ activo_p }}\">
                    <a href=\"javascript:;\" class=\"nav-link nav-toggle\">
                        <i class=\"{{ menu.icono }}\"></i>
                        <span class=\"title\">{{ menu.modulo }}</span>
                        <span class=\"arrow {{ row_open }}\"></span>
                    </a>
                    <ul class=\"sub-menu\">
                        {% for submenu in submenu(menu.id) %}
                            {% set activo = ''  %}
                            {% if submenu.id == _id %}
                                {% set activo = 'active open' %}
                            {% endif %}

                            <li class=\"nav-item {{ activo }}\">
                                <a href=\"{{base_url()~ submenu.route }}\" class=\"nav-link \">
                                    <span class=\"title\">{{ submenu.modulo }}</span>
                                </a>
                            </li>
                        {% endfor %}
                    </ul>
                </li>
            {% endfor %}
        </ul>
    </div>
</div>", "base/menu.twig", "/var/www/garajemotores/application/views/twig/templates/base/menu.twig");
    }
}
