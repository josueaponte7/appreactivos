<?php

/* registro/reactivos.twig */
class __TwigTemplate_48a442a9ddf542ae2e3a24bfb63347ffe50fb8037a1cfe359615934484c718a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base/base.twig", "registro/reactivos.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["forms"] = $this->loadTemplate("base/form.twig", "registro/reactivos.twig", 2);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    ";
        // line 5
        $this->displayBlock('stylesheets', $context, $blocks);
    }

    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "
    ";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"portlet-title\">
        <div class=\"caption font-red-sunglo\">
            <i class=\"icon-settings font-red-sunglo\"></i>
            <span class=\"caption-subject bold uppercase\"> ";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["titulo"]) ? $context["titulo"] : null), "html", null, true);
        echo "</span>
        </div>
        <div class=\"actions\">
            <div class=\"btn-group\">
                <a class=\"btn btn-sm green dropdown-toggle\" href=\"javascript:;\"
                data-toggle=\"dropdown\"> Accion
                <i class=\"fa fa-angle-down\"></i>
            </a>
            <ul class=\"dropdown-menu pull-right\">
                <li>
                    <a href=\"\" id=\"eliminar\">
                        <i class=\"fa fa-trash-o\"></i> Eliminar
                    </a>
                </li>
                </ul>
            </div>
        </div>
    </div>
    <div class=\"portlet-body form\">
        <div id=\"div-form\">
            ";
        // line 33
        echo $context["forms"]->getform_open("frmreactivo", "frmreactivo");
        echo "
            ";
        // line 34
        echo $context["forms"]->gettoken((isset($context["token"]) ? $context["token"] : null));
        echo "
            ";
        // line 35
        echo $context["forms"]->gettoken((isset($context["token"]) ? $context["token"] : null), "token");
        echo "
\t\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-12\">
\t\t\t\t";
        // line 38
        echo $context["forms"]->getlabel(array("id" => "materia_id", "class" => "control-label", "content" => "Materia"));
        echo "
\t\t\t\t<select name=\"materia_id\" id=\"materia_id\" class=\"form-control select2 select2-button-addons-single-input-group-sm\">
\t\t\t\t\t<option value=\"0\">Seleccione</option>
\t\t\t\t\t";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["materia"]) ? $context["materia"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["modulo"]) {
            // line 42
            echo "\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["modulo"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["modulo"], "materia", array()), "html", null, true);
            echo "</option>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['modulo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
            <div class=\"row\">
                <div class=\"col-md-6\">
                    ";
        // line 49
        echo $context["forms"]->getlabel(array("id" => "reactivo", "content" => "Reactivo"));
        echo "
                    ";
        // line 50
        echo $context["forms"]->getinput_text(array("attr" => array("name" => "reactivo", "class" => "form-control", "placeholder" => "Reactivo", "data" => array("validate" => "required|max(255)|min(4)", "type" => "alphaspace", "text" => "upper"))));
        echo "
                </div>
                <div class=\"col-md-6\">
                    <label for=\"span_small\" class=\"control-label\">Estatus</label>
                    <div class=\"clearfix\">
                        <div class=\"clearfix\">
                            <div class=\"btn-group btn-group-justified\" data-toggle=\"buttons\">
                                <label class=\"btn btn-default active\">Activo
                                    <input type=\"radio\" class=\"toggle\" name=\"activo\" value=\"1\" checked=\"checked\">
                                </label>
                                <label class=\"btn btn-default\">Inactivo
                                    <input type=\"radio\" class=\"toggle\" name=\"activo\" value=\"0\">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"form-actions\" style=\"text-align: center\">
            ";
        // line 70
        echo $context["forms"]->getbutton(array("attr" => array("id" => "guardar", "class" => "btn green uppercase")), "Guardar");
        echo "
            ";
        // line 71
        echo $context["forms"]->getbutton(array("attr" => array("id" => "cancelar", "class" => "btn yellow uppercase")), "Cancelar");
        echo "
        </div>
    </form>
    <table class=\"table  table-striped table-bordered table-hover table-checkable order-column\" id=\"treactivo\">
        <thead>
            <tr>
                <th>
                    <label class=\"mt-checkbox mt-checkbox-single mt-checkbox-outline\">
                        <input type=\"checkbox\" class=\"group-checkable\" data-set=\"#treactivo .checkboxes\"/>
                        <span></span>
                    </label>
                </th>
                <th>#</th>
                <th>Reactivo</th>
                <th>Estatus</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listar"]) ? $context["listar"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["lista"]) {
            // line 90
            echo "            ";
            $context["estatus"] = "Activo";
            // line 91
            echo "                ";
            if (($this->getAttribute($context["lista"], "activo", array()) == 0)) {
                // line 92
                echo "                ";
                $context["estatus"] = "Inactivo";
                // line 93
                echo "                ";
            }
            // line 94
            echo "                <tr class=\"odd gradeX\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["lista"], "id", array()), "html", null, true);
            echo "\" data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["lista"], "id", array()), "html", null, true);
            echo "\">
                    <td>
                        <label class=\"mt-checkbox mt-checkbox-single mt-checkbox-outline\">
                            <input type=\"checkbox\" class=\"checkboxes\" value=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["lista"], "id", array()), "html", null, true);
            echo "\"/>
                            <span></span>
                        </label>
                    </td>
                    <td>";
            // line 101
            echo twig_escape_filter($this->env, sprintf("%02d", $this->getAttribute($context["loop"], "index", array())), "html", null, true);
            echo "</td>
                    <td> ";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($context["lista"], "reactivo", array()), "html", null, true);
            echo " </td>
                    <td> ";
            // line 103
            echo twig_escape_filter($this->env, (isset($context["estatus"]) ? $context["estatus"] : null), "html", null, true);
            echo " </td>
                </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lista'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "        </tbody>
    </table>
</div>
";
    }

    // line 110
    public function block_javascript($context, array $blocks = array())
    {
        // line 111
        echo "
<script src=\"";
        // line 112
        echo twig_escape_filter($this->env, (public_url() . "script/reactivo.js"), "html", null, true);
        echo "\" type=\"text/javascript\" charset=\"utf-8\"></script>
";
    }

    public function getTemplateName()
    {
        return "registro/reactivos.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 112,  258 => 111,  255 => 110,  248 => 106,  231 => 103,  227 => 102,  223 => 101,  216 => 97,  207 => 94,  204 => 93,  201 => 92,  198 => 91,  195 => 90,  178 => 89,  157 => 71,  153 => 70,  130 => 50,  126 => 49,  119 => 44,  108 => 42,  104 => 41,  98 => 38,  92 => 35,  88 => 34,  84 => 33,  61 => 13,  56 => 10,  53 => 9,  48 => 6,  42 => 5,  37 => 4,  34 => 3,  30 => 1,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base/base.twig\" %}
{% import \"base/form.twig\" as forms %}
{% block head %}
    {{ parent() }}
    {% block stylesheets %}

    {% endblock %}
{% endblock %}
{% block content %}
    <div class=\"portlet-title\">
        <div class=\"caption font-red-sunglo\">
            <i class=\"icon-settings font-red-sunglo\"></i>
            <span class=\"caption-subject bold uppercase\"> {{ titulo }}</span>
        </div>
        <div class=\"actions\">
            <div class=\"btn-group\">
                <a class=\"btn btn-sm green dropdown-toggle\" href=\"javascript:;\"
                data-toggle=\"dropdown\"> Accion
                <i class=\"fa fa-angle-down\"></i>
            </a>
            <ul class=\"dropdown-menu pull-right\">
                <li>
                    <a href=\"\" id=\"eliminar\">
                        <i class=\"fa fa-trash-o\"></i> Eliminar
                    </a>
                </li>
                </ul>
            </div>
        </div>
    </div>
    <div class=\"portlet-body form\">
        <div id=\"div-form\">
            {{ forms.form_open('frmreactivo', 'frmreactivo') }}
            {{ forms.token(token) }}
            {{ forms.token(token,'token') }}
\t\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-12\">
\t\t\t\t{{ forms.label({'id':'materia_id', 'class':'control-label', 'content':'Materia'}) }}
\t\t\t\t<select name=\"materia_id\" id=\"materia_id\" class=\"form-control select2 select2-button-addons-single-input-group-sm\">
\t\t\t\t\t<option value=\"0\">Seleccione</option>
\t\t\t\t\t{% for modulo in materia %}
\t\t\t\t\t\t<option value=\"{{ modulo.id }}\">{{ modulo.materia }}</option>
\t\t\t\t\t{% endfor %}
\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
            <div class=\"row\">
                <div class=\"col-md-6\">
                    {{ forms.label({'id':'reactivo', 'content':'Reactivo'}) }}
                    {{ forms.input_text({'attr':{'name': 'reactivo', 'class':'form-control', 'placeholder':'Reactivo','data':{'validate':'required|max(255)|min(4)', 'type':'alphaspace', 'text':'upper'}}}) }}
                </div>
                <div class=\"col-md-6\">
                    <label for=\"span_small\" class=\"control-label\">Estatus</label>
                    <div class=\"clearfix\">
                        <div class=\"clearfix\">
                            <div class=\"btn-group btn-group-justified\" data-toggle=\"buttons\">
                                <label class=\"btn btn-default active\">Activo
                                    <input type=\"radio\" class=\"toggle\" name=\"activo\" value=\"1\" checked=\"checked\">
                                </label>
                                <label class=\"btn btn-default\">Inactivo
                                    <input type=\"radio\" class=\"toggle\" name=\"activo\" value=\"0\">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"form-actions\" style=\"text-align: center\">
            {{ forms.button({'attr':{'id':'guardar', 'class':'btn green uppercase'}}, 'Guardar') }}
            {{ forms.button({'attr':{'id':'cancelar', 'class':'btn yellow uppercase'}}, 'Cancelar') }}
        </div>
    </form>
    <table class=\"table  table-striped table-bordered table-hover table-checkable order-column\" id=\"treactivo\">
        <thead>
            <tr>
                <th>
                    <label class=\"mt-checkbox mt-checkbox-single mt-checkbox-outline\">
                        <input type=\"checkbox\" class=\"group-checkable\" data-set=\"#treactivo .checkboxes\"/>
                        <span></span>
                    </label>
                </th>
                <th>#</th>
                <th>Reactivo</th>
                <th>Estatus</th>
            </tr>
        </thead>
        <tbody>
            {% for lista in listar %}
            {% set estatus = 'Activo' %}
                {% if lista.activo == 0 %}
                {% set estatus = 'Inactivo' %}
                {% endif %}
                <tr class=\"odd gradeX\" id=\"{{ lista.id }}\" data-id=\"{{ lista.id }}\">
                    <td>
                        <label class=\"mt-checkbox mt-checkbox-single mt-checkbox-outline\">
                            <input type=\"checkbox\" class=\"checkboxes\" value=\"{{ lista.id }}\"/>
                            <span></span>
                        </label>
                    </td>
                    <td>{{ '%02d'|format(loop.index) }}</td>
                    <td> {{ lista.reactivo }} </td>
                    <td> {{ estatus }} </td>
                </tr>
            {% endfor %}
        </tbody>
    </table>
</div>
{% endblock %}
{% block javascript %}

<script src=\"{{ public_url()~ 'script/reactivo.js' }}\" type=\"text/javascript\" charset=\"utf-8\"></script>
{% endblock %}", "registro/reactivos.twig", "/var/www/appreactivos/application/views/twig/templates/registro/reactivos.twig");
    }
}
