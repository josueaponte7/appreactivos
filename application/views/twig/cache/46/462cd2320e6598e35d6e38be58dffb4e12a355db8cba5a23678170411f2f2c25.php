<?php

/* base/form.twig */
class __TwigTemplate_864c5c954f9d0be725910d123441d09cfec3fb63617f55c8bae93bd3d83bbfff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 5
        echo "

";
        // line 11
        echo "
";
        // line 15
        echo "
";
        // line 19
        echo "
";
        // line 23
        echo "
";
        // line 29
        echo "
";
        // line 39
        echo "
";
        // line 49
        echo "


";
    }

    // line 1
    public function getinput_text($__data__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "data" => $__data__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["aja"]) ? $context["aja"] : null), "placeholder", array()), "html", null, true);
            echo "
    <input type=\"text\" name=\"";
            // line 3
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "name", array()), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "name", array()), "html", null, true);
            echo "\" class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "class", array()), "html", null, true);
            echo "\" placeholder=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "placeholder", array()));
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "value", array()));
            echo "\" data-validate=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data", array()), "validate", array()));
            echo "\" data-type=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data", array()), "type", array()));
            echo "\" data-text=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data", array()), "text", array()));
            echo "\" data-add=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data", array()), "add", array()));
            echo "\" data-mod=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data", array()), "mod", array()));
            echo "\" style=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "style", array()));
            echo "\" size=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "size", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "size", array()), 100)) : (100)), "html", null, true);
            echo "\"/>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 7
    public function getinput_password($__data__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "data" => $__data__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 8
            echo "    ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["aja"]) ? $context["aja"] : null), "placeholder", array()), "html", null, true);
            echo "
    <input type=\"password\" name=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "name", array()), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "name", array()), "html", null, true);
            echo "\" class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "class", array()), "html", null, true);
            echo "\" placeholder=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "placeholder", array()));
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "value", array()));
            echo "\" data-validate=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data_validate", array()));
            echo "\" data-type=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data_type", array()));
            echo "\" data-text=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data_text", array()));
            echo "\" data-add=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data_add", array()));
            echo "\" data-mod=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "data_mod", array()));
            echo "\" style=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "style", array()));
            echo "\" size=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "size", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "size", array()), 100)) : (100)), "html", null, true);
            echo "\"/>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 12
    public function gettoken($__token__ = null, $__name__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "token" => $__token__,
            "name" => $__name__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 13
            echo "    <input type=\"hidden\" name=\"";
            echo twig_escape_filter($this->env, ((array_key_exists("name", $context)) ? (_twig_default_filter((isset($context["name"]) ? $context["name"] : null), "csrf_test_name")) : ("csrf_test_name")), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
            echo "\"/>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 16
    public function gettextarea($__name__ = null, $__value__ = null, $__class__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "name" => $__name__,
            "value" => $__value__,
            "class" => $__class__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 17
            echo "    <textarea name=\"";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "\" class=\"";
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null));
            echo "</textarea>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 20
    public function getlabel($__data__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "data" => $__data__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 21
            echo "    <label for=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "id", array()));
            echo "\" class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "class", array()));
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content", array()));
            echo "</label>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 24
    public function getbutton($__data__ = null, $__content__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "data" => $__data__,
            "content" => $__content__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 25
            echo "    <button type=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "type", array()), "button")) : ("button")), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "id", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "id", array()), "guardar")) : ("guardar")), "html", null, true);
            echo "\"  class=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "class", array()));
            echo "\" data-action=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "accion", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "accion", array()), "guardar")) : ("guardar")), "html", null, true);
            echo "\" data-accion=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "accion", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array(), "any", false, true), "accion", array()), "guardar")) : ("guardar")), "html", null, true);
            echo "\" style=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "attr", array()), "style", array()));
            echo "\">
        ";
            // line 26
            echo twig_escape_filter($this->env, ((array_key_exists("content", $context)) ? (_twig_default_filter((isset($context["content"]) ? $context["content"] : null), "Guardar")) : ("Guardar")), "html", null, true);
            echo "
    </button>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 30
    public function getselect_di($__name__ = null, $__element__ = null, $__style__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "name" => $__name__,
            "element" => $__element__,
            "style" => $__style__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 31
            echo "    <select name=\"";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "\" class=\"form-control select2\">
        <option value=\"0\">Seleccione</option>
        ";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["element"]);
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 34
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, $context["element"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["element"], "html", null, true);
                echo "</option>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "
    </select>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 40
    public function getselect($__name__ = null, $__element__ = null, $__value__ = null, $__text__ = null, $__style__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "name" => $__name__,
            "element" => $__element__,
            "value" => $__value__,
            "text" => $__text__,
            "style" => $__style__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 41
            echo "    <select name=\"";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
            echo "\" class=\"form-control select2 select2-button-addons-single-input-group-sm\">
        <option value=\"0\">Seleccione</option>
        ";
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["element"]);
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 44
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["element"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["element"], "modulo", array()), "html", null, true);
                echo "</option>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "
    </select>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 52
    public function getform_open($__id__ = null, $__class__ = null, $__action__ = null, $__method__ = null, $__style__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "id" => $__id__,
            "class" => $__class__,
            "action" => $__action__,
            "method" => $__method__,
            "style" => $__style__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 53
            echo "<form role=\"form\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null));
            echo "\" class=\"";
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : null));
            echo "\" action=\"";
            echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null));
            echo "\" method=\"";
            echo twig_escape_filter($this->env, (isset($context["method"]) ? $context["method"] : null));
            echo "\" style=\"";
            echo twig_escape_filter($this->env, (isset($context["style"]) ? $context["style"] : null));
            echo "\" >
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "base/form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  437 => 53,  421 => 52,  404 => 46,  393 => 44,  389 => 43,  381 => 41,  365 => 40,  348 => 36,  337 => 34,  333 => 33,  325 => 31,  311 => 30,  293 => 26,  278 => 25,  265 => 24,  243 => 21,  231 => 20,  207 => 17,  193 => 16,  173 => 13,  160 => 12,  121 => 9,  116 => 8,  104 => 7,  65 => 3,  60 => 2,  48 => 1,  41 => 49,  38 => 39,  35 => 29,  32 => 23,  29 => 19,  26 => 15,  23 => 11,  19 => 5,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% macro input_text(data) %}
    {{ aja.placeholder }}
    <input type=\"text\" name=\"{{ data.attr.name }}\" id=\"{{ data.attr.name }}\" class=\"{{ data.attr.class }}\" placeholder=\"{{ data.attr.placeholder|e }}\" value=\"{{ data.attr.value|e }}\" data-validate=\"{{ data.attr.data.validate|e }}\" data-type=\"{{ data.attr.data.type|e }}\" data-text=\"{{ data.attr.data.text|e }}\" data-add=\"{{ data.attr.data.add|e }}\" data-mod=\"{{ data.attr.data.mod|e }}\" style=\"{{ data.attr.style|e }}\" size=\"{{ data.attr.size|default(100) }}\"/>
{% endmacro %}


{% macro input_password(data) %}
    {{ aja.placeholder }}
    <input type=\"password\" name=\"{{ data.attr.name }}\" id=\"{{ data.attr.name }}\" class=\"{{ data.attr.class }}\" placeholder=\"{{ data.attr.placeholder|e }}\" value=\"{{ data.attr.value|e }}\" data-validate=\"{{ data.attr.data_validate|e }}\" data-type=\"{{ data.attr.data_type|e }}\" data-text=\"{{ data.attr.data_text|e }}\" data-add=\"{{ data.attr.data_add|e }}\" data-mod=\"{{ data.attr.data_mod|e }}\" style=\"{{ data.attr.style|e }}\" size=\"{{ data.attr.size|default(100) }}\"/>
{% endmacro %}

{% macro token(token, name) %}
    <input type=\"hidden\" name=\"{{ name|default('csrf_test_name') }}\" value=\"{{ token }}\"/>
{% endmacro %}

{% macro textarea(name, value, class) %}
    <textarea name=\"{{ name }}\" id=\"{{ name }}\" class=\"{{ class }}\">{{ value|e }}</textarea>
{% endmacro %}

{% macro label(data) %}
    <label for=\"{{ data.id|e }}\" class=\"{{ data.class|e }}\">{{ data.content|e }}</label>
{% endmacro %}

{% macro button(data, content) %}
    <button type=\"{{ data.attr.type|default('button') }}\" id=\"{{ data.attr.id|default('guardar') }}\"  class=\"{{ data.attr.class|e }}\" data-action=\"{{ data.attr.accion|default('guardar') }}\" data-accion=\"{{ data.attr.accion|default('guardar') }}\" style=\"{{ data.attr.style|e }}\">
        {{ content|default('Guardar') }}
    </button>
{% endmacro %}

{% macro select_di(name, element, style) %}
    <select name=\"{{ name }}\" id=\"{{ name }}\" class=\"form-control select2\">
        <option value=\"0\">Seleccione</option>
        {% for element in element %}
            <option value=\"{{ element }}\">{{ element }}</option>
        {% endfor %}

    </select>
{% endmacro %}

{% macro select(name, element, value, text, style) %}
    <select name=\"{{ name }}\" id=\"{{ name }}\" class=\"form-control select2 select2-button-addons-single-input-group-sm\">
        <option value=\"0\">Seleccione</option>
        {% for element in element %}
            <option value=\"{{ element.id }}\">{{ element.modulo }}</option>
        {% endfor %}

    </select>
{% endmacro %}



{% macro form_open(id, class, action, method ,style) %}
<form role=\"form\" id=\"{{ id|e }}\" class=\"{{ class|e }}\" action=\"{{ action|e }}\" method=\"{{ method|e }}\" style=\"{{ style|e }}\" >
{% endmacro %}", "base/form.twig", "/var/www/garajemotores/application/views/twig/templates/base/form.twig");
    }
}
