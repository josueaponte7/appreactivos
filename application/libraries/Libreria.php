<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '512M');
define('EXTE', '.php');

class Libreria
{

    protected $ci;
    public function __construct()
    {
        $this->ci =& get_instance();
        if(!isset($this->ci->session)) {
            $this->ci->load->library('session');
        }
        $this->ci->load->library('encryption');
        $encripinit = array('cipher' => 'aes-256', 'mode' => 'ctr', 'key' => '2ye4r*xbWRN?G$_sa3j@uya-w&KgJC#V');
        $this->ci->encryption->initialize($encripinit);
    }

    public function menu()
    {

        if(!$this->ci->session->logged_in){
            redirect(base_url());
        }
        $user_id   = $this->ci->session->userdata('user_id');
        $perfil_id = $this->ci->session->userdata('perfil_id');

        $this->ci->db->select('m.id, m.modulo, m.modulo_id, m.route, m.icono');
        $this->ci->db->from('se_modulo as m');
        $this->ci->db->join('se_permissions p','m.id=p.modulo_id','inner');
        $this->ci->db->where('m.modulo_id', 0);
        $this->ci->db->where('m.activo', TRUE);
        $this->ci->db->where('(p.perfil_id='. $perfil_id.' OR p.user_id='.$user_id.')');
        $this->ci->db->group_by(array("m.id"));
        $this->ci->db->order_by("m.posicion", "asc");
        $query = $this->ci->db->get();

        return $query->result();
    }


    public function submenu($id)
    {
        $user_id   = $this->ci->session->user_id;
        $perfil_id = $this->ci->session->perfil_id;

        $this->ci->db->select('m.id, m.modulo, m.modulo_id, m.route, m.icono');
        $this->ci->db->from('se_modulo as m');
        $this->ci->db->join('se_permissions p','m.id=p.modulo_id','inner');
        $this->ci->db->where('m.modulo_id', $id);
        $this->ci->db->where('m.activo', TRUE);
        $this->ci->db->where('(p.perfil_id='. $perfil_id.' OR p.user_id='.$user_id.')');
        $this->ci->db->group_by(array("m.id"));
        $this->ci->db->order_by("m.posicion", "asc");
        $query = $this->ci->db->get();

        return $query->result();
    }

    public function modulo()
    {
        $this->ci->db->select('id, modulo, modulo_id');
        $this->ci->db->order_by("posicion", "asc");
        $this->ci->db->where_in('modulo_id', 0);
        $query = $this->ci->db->get('se_modulo');
        return $query->result();
    }

    public function submodulo($id)
    {
        $this->ci->db->select('id, modulo');
        $this->ci->db->where_in('modulo_id', $id);
        $this->ci->db->order_by("posicion", "asc");
        $query = $this->ci->db->get('se_modulo');
        return $query->result();
    }

    public function getName()
    {
        $user_id = $this->ci->session->user_id;

        $this->ci->db->select("si_user, first_name, last_name, show_panel");
        $this->ci->db->where("id",$user_id);
        $query = $this->ci->db->get('se_users',1);
        $row = $query->row();
        if($row->show_panel == 1){
            $name_show = $row->si_user;
        }else{
            $name_show = $row->first_name .' '.$row->last_name;
        }
        return strtoupper($name_show);
    }

    public function getNamesUser()
    {
        $user_id = $this->ci->session->user_id;

        $this->ci->db->select("si_user, first_name, last_name, show_panel");
        $this->ci->db->where("id",$user_id);
        $query = $this->ci->db->get('se_users',1);
        return $query->row();
    }

    public function getPerfil()
    {
        $perfil_id = $this->ci->session->userdata('perfil_id');;
        $this->ci->db->select("perfil");
        $this->ci->db->where("id",$perfil_id);
        $query = $this->ci->db->get('se_perfil',1);
        $row = $query->row();
        $perfil = $row->perfil;
        return strtoupper($perfil);
    }

    public function token()
    {
        $token = $this->ci->security->get_csrf_hash();
        $this->ci->session->set_userdata('token',$token);
        return $token;
    }

    public function checkPassword($user, $pass)
    {

        $this->ci->db->select("id, si_password, perfil_id");
        $this->ci->db->where("si_user",$user);
        $this->ci->db->or_where('correo',$user);
        $query = $this->ci->db->get('se_users',1);
        if($query->num_rows() > 0){
            $row = $query->row();
            $passdb = $this->ci->encryption->decrypt($row->si_password);
            if($pass === $passdb){
                $id        = $query->row()->id;
                $perfil_id = $query->row()->perfil_id;

                $sesiones = array('perfil_id'=>$perfil_id,'user_id' => $id , 'logged_in' => TRUE );
                $this->ci->session->set_userdata($sesiones);
                return TRUE;
            }else{
                return FALSE;
            }
        }
    }
    public function generatePassword($text)
    {
        $ciphertext = $this->ci->encryption->encrypt($text);
        return $ciphertext;
    }

    public function generateActivity($activity, $module_id=NULL)
    {
        date_default_timezone_set('America/Caracas');
        $user_id = $this->ci->session->userdata('user_id');
        $data['activity']      = $activity;
        $data['date_activity'] = date('Y-m-d H:i:s');;
        $data['modulo_id']     = $module_id;
        $data['user_id']       = $this->ci->session->userdata('user_id');
        return $this->ci->db->insert('se_activity', $data);
    }

    public function formatDate($date,$format)
    {
        return  date($format,strtotime($date));
    }

    public function edad($fecha)
    {
        list($ano,$mes,$dia) = explode("-",$fecha);
        $ano_diferencia  = date("Y") - $ano;
        $mes_diferencia = date("m") - $mes;
        $dia_diferencia   = date("d") - $dia;
        if ($dia_diferencia < 0 || $mes_diferencia < 0){
            $ano_diferencia--;
        }
        return $ano_diferencia;
    }

    public function save_file($input_file, $nombre, $ruta_file)
    {
        $temp   = $_FILES[$input_file]['tmp_name'];
        $ext    = explode('.',$_FILES[$input_file]['name']);
        $ext    = end($ext);

        $config['upload_path']   = 'assets/'.$ruta_file;
        $config['file_name']     = $nombre.'.'.$ext;
        $config['allowed_types'] = 'gif|jpg|png';
        // $config['max_size']      = 100;
        // $config['max_width']     = 1024;
        // $config['max_height']    = 768;

        $bandera = TRUE;

        $this->ci->load->library('upload',$config);

        $this->ci->upload->initialize($config);
        if ( !$this->ci->upload->do_upload($input_file)){
            $bandera = FALSE;
        }

        return $bandera;
    }

    public function save_multiple_file($input_file, $nombre, $ruta_foto)
    {

        $config['upload_path']   = 'assets/'.$ruta_foto;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['overwrite'] = TRUE;
        // $config['file_name']     = $cedula.'.'.$ext;
        // $config['max_size']      = 100;
        // $config['max_width']     = 1024;
        // $config['max_height']    = 768;

        $this->ci->load->library('upload',$config);
        $files = $_FILES;

        $cpt = count ($_FILES [$input_file] ['name']);
        $j = 1;
        $bandera = TRUE;
        for($i = 0; $i < $cpt; $i ++) {

            $_FILES [$input_file] ['name']     = $files ['foto'] ['name'] [$i];
            $_FILES [$input_file] ['type']     = $files [$input_file] ['type'] [$i];
            $_FILES [$input_file] ['tmp_name'] = $files [$input_file] ['tmp_name'] [$i];
            $_FILES [$input_file] ['error']    = $files [$input_file] ['error'] [$i];
            $_FILES [$input_file] ['size']     = $files [$input_file] ['size'] [$i];

            $ext    = explode('.',$_FILES[$input_file]['name']);
            $ext    = end($ext);

            $config['file_name']     = $nombre."_$j.".$ext;
            $this->ci->upload->initialize ($config);
            if (!$this->ci->upload->do_upload($input_file)){
                $bandera = FALSE;
                break;
            }
            $j++;
        }

        return $bandera;
    }

    public function getControllers($dir='api')
    {
        foreach(glob(APPPATH . "controllers/$dir") as $controller) {
            if(is_dir($controller)) {
                $dirname = basename($controller, EXTE);
                $arr_controller = [];
                foreach(glob(APPPATH . 'controllers/'.$dirname.'/*') as $subdircontroller) {
                    $subdircontrollername = basename($subdircontroller, EXTE);
                    $arr_controller[] =  $subdircontrollername;
                }
            }
        }
        return $arr_controller;
    }

    public function getModuleID($controller)
    {
        $this->ci->db->select("id");
        $this->ci->db->where("controller",$controller);
        $query = $this->ci->db->get('se_modulo',1);
        $row = $query->row();
        return $row->id;
    }

    public function getPadreID($id)
    {
        $this->ci->db->select("modulo_id");
        $this->ci->db->where("id", $id);
        $query = $this->ci->db->get('se_modulo', 1);
        $row = $query->row();
        return $row->modulo_id;
    }
}
